import { FormControl, FormGroup } from "@angular/forms";
import { RegexConstant } from "../constant/regex";

export const trimData = (object) => {
	for (let property in object) {
		//   if (typeof object[property] === 'object') {
		//     object[property] = this.trimData(object[property])
		//   } else
		if (typeof object[property] === "string" && object[property]) {
			object[property] = object[property].trim();
		}
	}
	return object;
};

export const markAsTouched = (group: FormGroup) => {
	group.markAsTouched({ onlySelf: true });
	Object.keys(group.controls).map((field) => {
		const control = group.get(field);
		if (control instanceof FormControl) {
			control.markAsTouched({ onlySelf: true });
		} else if (control instanceof FormGroup) {
			markAsTouched(control);
		}
	});
};

export const getCurrentTimeSigned = () => {
	const currentTime = new Date().getTime() + "";
	const ind = Math.floor(Math.random() * (currentTime.length - 2));
	return `${ind}${currentTime}${currentTime.substr(ind, 2)}`;
};

export const matchingPasswords = (passwordKey: string, passwordConfirmationKey: string) => {
	return (group: FormGroup) => {
		let password = group.controls[passwordKey];
		let passwordConfirmation = group.controls[passwordConfirmationKey];
		if (password.value !== passwordConfirmation.value) {
			return passwordConfirmation.setErrors({ mismatchedPasswords: true });
		}
	};
};

export const checkFileType = (fileName: string, fileTypes: any) => {
	if (fileName) {
		const typeFile = fileName.substr(fileName.lastIndexOf(".") + 1, fileName.length).toLowerCase();
		return fileTypes.includes(typeFile);
	}
};

export function emailAllValidator(emailKey: string) {
    var emailRegexp = RegexConstant.REGEX_ALL_EMAIL;
    return (group: FormGroup) => {
      let email = group.controls[emailKey];
      if (email.value && !emailRegexp.test(email.value)) {
        return email.setErrors({ invalidEmail: true });
      }
    };
  }
