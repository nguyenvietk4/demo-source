import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _user: BehaviorSubject<any> = new BehaviorSubject({});
  public get user(): any {
    return this._user.value;
  }
  public user$: Observable<any> = this._user.asObservable();

  constructor() {
  }

  public updateUserBehaviorSubject(user: any) {
    this._user.next(user);
  }
}
