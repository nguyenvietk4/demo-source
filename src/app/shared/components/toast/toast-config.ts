import { InjectionToken, TemplateRef } from '@angular/core';

export class ToastData {
  conversationId?: string;
  title?: string;
  type: ToastType;
  text?: string;
  template?: TemplateRef<any>;
  templateContext?: {};
  messageData?: object;
}

export type ToastType = 'warning' | 'info' | 'success';

export interface ToastConfig {
  position?: {
    top: number;
    right: number;
  };
  animation?: {
    fadeOut: number;
    fadeIn: number;
  };
}

export const defaultToastConfig: ToastConfig = {
  position: {
    top: 60,
    right: 16,
  },
  animation: {
    fadeOut: 100,
    fadeIn: 300,
  },
};

export const TOAST_CONFIG_TOKEN = new InjectionToken('toast-config');
