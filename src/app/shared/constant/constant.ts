export class Constant {
	// Key localStorage
	public static key_local_token = "token";
	public static key_local_version = "version";
	public static key_local_language = "language";
	public static key_local_status = "status";

	// Error code
	public static error_force_logout = 401;
	public static error_not_connect_server = 0;
	public static error_server_error = 500;
	public static error_server_maintenance = 503;
	public static error_code = 400;

	// Success code
	public static success_code = 200;
	// Request timeout
	public static request_time_out = 60000;

	// Mobile Version
	public static MOBILE_VERSION = "2.0.0";

	public static FAKE_DATA = [
		{
			"userId": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9",
			"userName": "AD01",
			"password": "12345678",
			"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
			"email": "ad01@gmail.com",
			"firstName": "admin",
			"lastName": "01",
			"address": "An Đổ, Bình Lục, Hà Nam",
			"city": "Hà Nam",
			"country": "Việt Nam",
			"postalCode": "7000",
			"description": "Là Admin",
			"role": "Admin",
			"permissions": []
		},
		{
			"userId": "eySflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV",
			"userName": "user01",
			"password": "12345678",
			"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
			"email": "user01@gmail.com",
			"firstName": "user",
			"lastName": "01",
			"address": "An Đổ, Bình Lục, Hà Nam",
			"city": "Hà Nam",
			"country": "Việt Nam",
			"postalCode": "7000",
			"description": "Là User",
			"role": "User",
			"permissions": []
		}
	]
}
