export class ErrorMessage {
    public static ERROR_REQUIRED = 'Bạn chưa nhập thông tin';
    public static ERROR_INVALID = 'Thông tin không hợp lệ';
    public static ERROR_MAXLENGTH = 'Độ dài ký tự không được vượt quá ';
    public static ERROR_MAX_VALUE = 'Không được lớn hơn ';
    public static ERROR_INVALID_EMAIL = 'Email không hợp lệ';
    public static ERROR_MINLENGTH = 'Độ dài tối thiêu ';
    public static ERROR_INVALID_PASSWORD = 'Mật khẩu phải bao gồm ít nhất một kí tự in thường, một ký tự in hoa, một ký tự số và một ký tự đặc biệt [$@$!%*?&]';
    public static ERROR_MATCHING_PASSWORD = 'Mật khẩu không trùng khớp';
    public static ERROR_FORM_REQUIRED = 'Bạn chưa nhập đầy đủ thông tin';
};
