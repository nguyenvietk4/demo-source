export class RegexConstant {
	public static REGEX_EMAIL = /^[a-z][a-z0-9_\.]{1,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$/gm;
	public static REGEX_ALL_EMAIL = /^(([^<>()[\]\\.,;:@\']+(\.[^<>()[\]\\.,;:@\']+)*)|(\'.+\'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9\s]+\.)+[a-zA-Z\s]{2,}))$/;
	public static REGEX_YOUTUBE_URL = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
	public static REGEX_INTERNAL_URL = /^https?:\/\/(.+\/)+.+(\.(mp4))$/;

	public static REGEX_TEXT = /^ *[^0-9 ]+.*$/;
	public static REGEX_ONLY_TEXT_VIETNAMESE = /^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$/;
	public static REGEX_ANSWER_SURVEY = /[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g;

	public static REGEX_NUMBER = /^[0-9]*$/;
	public static REGEX_NUMBER_ELDER_ZERO = /^[1-9][0-9]{0,9}$/;
	public static REGEX_VN_PHONE = /((09|03|07|08|05|849|843|847|848|845)+([0-9]{8})\b)/;
	public static REGEX_PASSWORD = /^[A-Za-z\d!@#$%^&*]/;
	public static REGEX_VALIDATE_FLOAT = "^\\d*(\\.\\d+)?$";
	public static REGEX_NOTE_ADMIN = /[\r\n\x0B\x0C\u0085\u2028\u2029]+/g; // Xóa xuống dòng, xóa khoảng trắng thừa
	public static REGEX_VN_TEXT = /^[aAàÀảẢãÃáÁạẠăĂằẰẳẲẵẴắẮặẶâÂầẦẩẨẫẪấẤậẬbBcCdDđĐeEèÈẻẺẽẼéÉẹẸêÊềỀểỂễỄếẾệỆfFgGhHiIìÌỉỈĩĨíÍịỊjJkKlLmMnNoOòÒỏỎõÕóÓọỌôÔồỒổỔỗỖốỐộỘơƠờỜởỞỡỠớỚợỢpPqQrRsStTuUùÙủỦũŨúÚụỤưƯừỪửỬữỮứỨựỰvVwWxXyYỳỲỷỶỹỸýÝỵỴzZ0-9 ]*$/;
	public static REGEX_CODE = /^[a-zA-Z0-9 -]*$/;
}
