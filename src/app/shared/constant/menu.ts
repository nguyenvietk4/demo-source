declare const $: any;
declare interface MenuInfo {
	path: string;
	title: string;
	class: string;
}
export const MENU_NAVIGATION: MenuInfo[] = [
	{ path: "/home", title: "Home", class: "" },
	{ path: "/machine-setup", title: "Machine Setup", class: "" },
	{ path: "/comparison", title: "Comparison", class: "" },
	{ path: "/report", title: "Report", class: "" },
];