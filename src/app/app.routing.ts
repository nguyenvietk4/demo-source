import { RegisterComponent } from './pages/register/register.component';
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LayoutComponent } from "./pages/layouts/layout/layout.component";
import { LoginComponent } from "./pages/login/login.component";
import { UserRouteAccessDeactivate } from "./shared/services/common/user-route-access.service";

const routes: Routes = [
	{
		path: "",
		redirectTo: "home",
		pathMatch: "full",
	},
	{
		path: "login",
		component: LoginComponent,
		pathMatch: "full",
	},
	{
		path: "register",
		component: RegisterComponent,
		pathMatch: "full",
	},
	{
		path: "",
		component: LayoutComponent,
		canActivate: [UserRouteAccessDeactivate],
		children: [
			{
				path: "home",
				loadChildren: () => import("./pages/user-profile/user-profile.module").then((m) => m.UserProfileModule),
			},
		],
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
