import { RegisterModule } from "./pages/register/register.module";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app.routing";
import { LayoutModule } from "./pages/layouts/layout.module";
import { LoginModule } from "./pages/login/login.module";
import { ToastrModule } from "ngx-toastr";
import { ToastModule } from "./shared/components/toast";

@NgModule({
	imports: [
		BrowserAnimationsModule,
		FormsModule,
		ReactiveFormsModule,
		HttpClientModule,
		LayoutModule,
		AppRoutingModule,
		LoginModule,
		RegisterModule,
		ToastrModule.forRoot({
			timeOut: 2000,
			positionClass: "toast-top-right",
			progressAnimation: "increasing",
			maxOpened: 3,
		}), // ToastrModule added
		ToastModule.forRoot({
			position: {
				top: 20,
				right: 16,
			},
			animation: {
				fadeOut: 100,
				fadeIn: 50,
			},
		}),
	],
	declarations: [AppComponent],
	providers: [],
	bootstrap: [AppComponent],
})
export class AppModule {}
