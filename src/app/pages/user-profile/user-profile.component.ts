import { StorageService } from "./../../shared/services/storage.service";
import { Constant } from "./../../shared/constant/constant";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";

@Component({
	selector: "app-user-profile",
	templateUrl: "./user-profile.component.html",
	styleUrls: ["./user-profile.component.scss"],
})
export class UserProfileComponent implements OnInit {
	formGroup: FormGroup = null;
	fake_data = Constant.FAKE_DATA;
	user: any = {
		userId: "",
		userName: "",
		email: "",
		firstName: "",
		lastName: "",
		address: "",
		city: "",
		country: "",
		postalCode: "",
		description: "",
	};

	constructor(public storageService: StorageService, private formBuilder: FormBuilder) {}

	ngOnInit() {
		const userId = this.storageService.retrieve("userId");
		let userFind: any = this.fake_data.find((data) => data.userId === userId);
		if (!userFind) {
			userFind = {
				userId: "",
				userName: "",
				email: "",
				firstName: "",
				lastName: "",
				address: "",
				city: "",
				country: "",
				postalCode: "",
				description: "",
			};
			userFind.userName = this.storageService.retrieve('userName');
		}
		this.user = userFind;
		this.initForm();
	}

	initForm() {
		this.formGroup = this.formBuilder.group({
			userName: new FormControl(this.user.userName),
			email: new FormControl(this.user.email),
			firstName: new FormControl(this.user.firstName),
			lastName: new FormControl(this.user.lastName),
			address: new FormControl(this.user.address),
			city: new FormControl(this.user.city),
			country: new FormControl(this.user.country),
			postalCode: new FormControl(this.user.postalCode),
			description: new FormControl(this.user.description),
		});
	}
}
