import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RegisterComponent } from "../register/register.component";
import { RegisterRoutes } from "./register.routing";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
	imports: [
		CommonModule,
		RouterModule.forChild(RegisterRoutes),
		FormsModule,
		ReactiveFormsModule,
	],
	declarations: [
		RegisterComponent
	],
})
export class RegisterModule { }
