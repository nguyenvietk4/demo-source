import { RegisterComponent } from './register.component';
import { Routes } from "@angular/router";

export const RegisterRoutes: Routes = [{ path: "", component: RegisterComponent, pathMatch: 'full' }];
