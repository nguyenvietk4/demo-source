import { emailAllValidator, markAsTouched, matchingPasswords } from "./../../shared/ultility/index";
import { ToastrService } from "ngx-toastr";
import { AuthService } from "./../../../services/auth.service";
import { StorageService } from "./../../shared/services/storage.service";
import { EventChangeService } from "./../../shared/services/event.change.service";
import { ErrorMessage } from "./../../shared/constant/error.message";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
	selector: "app-register",
	templateUrl: "./register.component.html",
	styleUrls: ["./register.component.scss"],
})
export class RegisterComponent implements OnInit {
	formGroup: FormGroup = null;
	validatorMinLength = 6;
	errorMessage = ErrorMessage;

	constructor(private formBuilder: FormBuilder, public router: Router, private eventChangeService: EventChangeService, public storageService: StorageService, public authService: AuthService, public toastrService: ToastrService) {}

	ngOnInit(): void {
		this.initForm();
	}

	getFormControl(name: string) {
		return this.formGroup && this.formGroup.get(name);
	}

	initForm() {
		this.formGroup = this.formBuilder.group(
			{
				username: new FormControl(null, Validators.required),
				email: new FormControl(null, [Validators.required, Validators.email]),
				password: new FormControl(null, [Validators.required, Validators.minLength(this.validatorMinLength)]),
				confirmPassword: new FormControl(null, [Validators.required, Validators.minLength(this.validatorMinLength)]),
			},
			{
				validators: [matchingPasswords("password", "confirmPassword"), emailAllValidator("email")],
			},
		);
	}

	async onSubmit() {
		markAsTouched(this.formGroup);
		if (!this.formGroup.valid) return this.toastrService.error("Kiểm tra lại thông tin");
		const body = this.formGroup.getRawValue();
		console.log(body);
		await this.authService.register({ userName: body.username });
		const url = this.storageService.retrieve("callbackUrl") || "/user-profile";
		this.router.navigate([url]);
		this.storageService.remove("callbackUrl");
	}
}
