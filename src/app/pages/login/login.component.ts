import { markAsTouched } from './../../shared/ultility/index';
import { Constant } from "./../../shared/constant/constant";
import { ToastrService } from "ngx-toastr";
import { AuthService } from "./../../../services/auth.service";
import { StorageService } from "./../../shared/services/storage.service";
import { EventChangeService } from "./../../shared/services/event.change.service";
import { ErrorMessage } from "./../../shared/constant/error.message";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
	selector: "app-login",
	templateUrl: "./login.component.html",
	styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
	formGroup: FormGroup = null;
	fake_data = Constant.FAKE_DATA;
	errorMessage = ErrorMessage;

	constructor(private formBuilder: FormBuilder, public router: Router, private eventChangeService: EventChangeService, public storageService: StorageService, public authService: AuthService, public toastrService: ToastrService) {}

	ngOnInit(): void {
		this.initForm();
	}

	initForm() {
		this.formGroup = this.formBuilder.group({
			username: new FormControl(null, Validators.required),
			password: new FormControl(null, [Validators.required, Validators.minLength(6)]),
		});
	}

	getFormControl(name: string) {
		return this.formGroup && this.formGroup.get(name);
	}

	async onSubmit() {
		markAsTouched(this.formGroup);
		if (!this.formGroup.valid) return this.toastrService.error("Kiểm tra lại thông tin");

		const body = this.formGroup.getRawValue();
		const user = this.fake_data.find((data) => {
			if (data.userName === body.username && data.password === body.password) {
				return data;
			}
		});
		console.log(user);
		if (!user) return this.toastrService.error("Tài khoản không tồn tại");

		await this.authService.login(user);
		const url = this.storageService.retrieve("callbackUrl") || "/user-profile";
		this.router.navigate([url]);
		this.storageService.remove("callbackUrl");
	}
}
