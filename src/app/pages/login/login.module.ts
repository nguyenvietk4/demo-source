import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LoginComponent } from "../login/login.component";
import { LoginRoutes } from "./login.routing";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
	imports: [
		CommonModule,
		RouterModule.forChild(LoginRoutes),
		FormsModule,
		ReactiveFormsModule,
	],
	declarations: [
		LoginComponent
	],
})
export class LoginModule { }
