import { MENU_NAVIGATION } from '../../../shared/constant/menu';
import { Component, OnInit } from "@angular/core";

@Component({
	selector: "app-sidebar",
	templateUrl: "./sidebar.component.html",
	styleUrls: ["./sidebar.component.scss"],
})
export class SidebarComponent implements OnInit {
	menuItems: any[];

	constructor() {}

	ngOnInit() {
		this.menuItems = MENU_NAVIGATION.filter((menuItem) => menuItem);
	}
}
