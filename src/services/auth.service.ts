import { Injectable } from "@angular/core";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/Rx";
import { SecurityService, StorageService } from "../app/shared/services";

@Injectable({
	providedIn: "root",
})
export class AuthService {
	isUnAuthen = false;
	// url: string = ConstantUrl.url_auth + '/';

	constructor(private securityService: SecurityService, private storageService: StorageService) {}

	login(body) {
		this.storageService.store("userId", body.userId);
		this.storageService.store("userName", body.userName);
		this.storageService.store("role", body.role);
		this.storageService.store("permissions", body.permissions);
		this.securityService.setAuthorizationData(body.token, null, body.expires_in);
		return body;
	}

	register(body) {
		body.userId = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXV999',
		body.role = 'User',
		body.permissions = [],
		body.token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
		this.storageService.store("userId", body.userId);
		this.storageService.store("userName", body.userName);
		this.storageService.store("role", body.role);
		this.storageService.store("permissions", body.permissions);
		this.securityService.setAuthorizationData(body.token, null, body.expires_in);
		return body;
	}
}
